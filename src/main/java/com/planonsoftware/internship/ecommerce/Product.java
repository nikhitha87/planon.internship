package com.planonsoftware.internship.ecommerce;

/**
 * Product class contains product details like id,name,quantity and price
 * 
 * @author Nikhitha
 *
 */
public class Product {

	private int pid, quantity;
	private String pName;
	private double price;

	/**
	 * Constructor used for intializing variables
	 * 
	 * @param pid
	 * @param pName
	 * @param quantity
	 * @param price
	 */
	public Product(int pid, String pName, int quantity, double price) {
		this.pid = pid;
		this.pName = pName;
		this.quantity = quantity;
		this.price = price;
	}

	public Product() {
	}

	/**
	 * sets the id
	 * 
	 * @param pid
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}

	/**
	 * @return returns the id
	 */
	public int getPid() {
		return pid;
	}

	/**
	 * sets the name
	 * 
	 * @param pName
	 */
	public void setPName(String pName) {
		this.pName = pName;
	}

	/**
	 * @return returns Product name
	 */
	public String getPName() {
		return pName;
	}

	/**
	 * Sets the quantity
	 * 
	 * @param quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return returns quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * sets the price
	 * 
	 * @param price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return returns the price
	 */
	public double getPrice() {
		return price;
	}

}
