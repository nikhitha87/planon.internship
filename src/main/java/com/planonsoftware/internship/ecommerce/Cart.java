package com.planonsoftware.internship.ecommerce;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cart class contains cart products
 * 
 * @author Nikhitha
 *
 */
public class Cart {

	/**
	 * cart contains cart products
	 */
	private Map<Product, Integer> cart;
	/**
	 * totalprice is the price of all products in a cart
	 */
	private double totalPrice;

	public Cart() {
		cart = new HashMap<>();
		totalPrice = 0.0;
	}

	/**
	 * getProductByProductID returns Product based on id
	 * 
	 * @param pid
	 * @return
	 * @throws IOException
	 */
	public Product getProductByProductID(int pid) throws Exception {
		List<Product> products = FileDetails.getInstance().getProducts();
		for (Product product : products) {
			if (product.getPid() == pid) {
				return product;
			}
		}
		return null;
	}

	/**
	 * addToCart adds the product and quantity to the HashMap
	 * 
	 * @param p
	 * @param quantity
	 */
	public void addToCart(Product product, int quantity) {

		if (cart.containsKey(product)) {
			cart.put(product, cart.get(product) + quantity);
		} else {
			cart.put(product, quantity);
		}
		totalPrice += (product.getPrice() * quantity);
	}

	/**
	 * Displays the cart details
	 */
	public void cartDisplay() {
		if (cart.size() == 0) {
			System.out.println("Your cart is empty");
		}
		for (Product product : cart.keySet()) {
			System.out.println(product.getPid() + " " + product.getPName() + " " + cart.get(product) + " "
					+ product.getPrice() * cart.get(product));
		}
		System.out.println("Total Price is:" + totalPrice);
	}
}
