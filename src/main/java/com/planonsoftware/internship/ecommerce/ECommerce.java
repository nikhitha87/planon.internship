package com.planonsoftware.internship.ecommerce;

import java.io.IOException;
import java.util.Scanner;

/**
 * This Class takes the input and returns the cart details and total price
 * 
 * @author Nikhitha
 *
 */
public class ECommerce {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String args[]) throws Exception {
		ECommerce ecommerce = new ECommerce();
		ecommerce.shop();
	}

	/**
	 * reads details from file and displays result
	 * 
	 * @throws IOException
	 */
	private void shop() throws Exception {
		FileDetails fileDetails = FileDetails.getInstance();
		fileDetails.printProducts();
		Cart cart = new Cart();
		input(cart);
		cart.cartDisplay();
	}

	/**
	 * collecting input
	 * 
	 * @param cart
	 * @throws IOException
	 */
	private void input(Cart cart) throws Exception {
		print("Select the product from the above products");
		collectProductDetails(cart);
	}

	/**
	 * collecting product details from user
	 * 
	 * @param cart
	 * @throws IOException
	 */
	private void collectProductDetails(Cart cart) throws Exception {
		try (Scanner scanner = new Scanner(System.in)) {
			char ch;
			do {
				Product product;
				print("Enter ProductId:");
				int id = scanner.nextInt();
				product = cart.getProductByProductID(id);
				print("Enter Quantity");
				int quantity = scanner.nextInt();
				// adding to cart
				cart.addToCart(product, quantity);
				print("Do you want to continue?");
				ch = scanner.next().charAt(0);
			} while (ch == 'Y' || ch == 'y');
		}
	}

	/**
	 * prints the message
	 * 
	 * @param message
	 */
	private void print(String message) {
		System.out.println(message);
	}

}
