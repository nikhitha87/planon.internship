package com.planonsoftware.internship.ecommerce;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikhitha
 *
 */
public class FileDetails {

	private static FileDetails fileDetails;

	// returns instance of FileDetails if object is not created
	public static FileDetails getInstance() throws Exception {
		if (fileDetails == null) {
			fileDetails = new FileDetails();
		}
		return fileDetails;
	}

	/**
	 * Products is the list of products read from text file
	 */

	private List<Product> products = new ArrayList<>(); // One line comment

	/**
	 * @return returns the list of products
	 */
	public List<Product> getProducts() {
		return products;
	}

	// prints the details of text read from file
	public void printProducts() {
		System.out.println("Products available are:");
		products.forEach((product) -> {
			System.out.println(product.getPid() + " " + product.getPName() + " " + product.getQuantity() + " "
					+ product.getPrice() + " ");
		});
	}

	/**
	 * Reads the text from file
	 * 
	 * @throws IOException
	 */
	private FileDetails() throws Exception {
		InputStream in=this.getClass().getResourceAsStream("\\products.txt");
				BufferedReader buffer = new BufferedReader(new InputStreamReader(in)) ;
			String s;
			while ((s = buffer.readLine()) != null) {
				String[] data = s.split(",");
				// adding the data into list
				Product product = new Product(Integer.parseInt(data[0]), data[1], Integer.parseInt(data[2]),
						Double.parseDouble(data[3]));
				products.add(product);
			}
			buffer.close();
		}
	}
