package com.planonsoftware.internship.studentdetails;

import java.io.IOException;

/**
 * This class reads the StudentDetails file and prints number of records from
 * the file
 * 
 * @author Nikhitha
 *
 */
public class Main {
	public static void main(String[] args) throws IOException {
		StudentReader studentReader = new StudentReader("StudentDetails");
		studentReader.readFile();
	}
}
