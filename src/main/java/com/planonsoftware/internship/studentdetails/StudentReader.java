package com.planonsoftware.internship.studentdetails;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.planonsoftware.internship.studentdetails.recorddetails.IDetails;
import com.planonsoftware.internship.studentdetails.recorddetails.RecordDetailer;
import com.planonsoftware.internship.studentdetails.recorddetails.recordtype.College;
import com.planonsoftware.internship.studentdetails.recorddetails.recordtype.Department;
import com.planonsoftware.internship.studentdetails.recorddetails.recordtype.Student;

public class StudentReader {

	private String filename;
	public static int successfulRecords = 0;// variable for successful records
	public static int failureRecords = 0;// variable for failure records
	RecordDetailer record = null;

	/**
	 * returns the instance of RecordDetailer
	 * 
	 * @return
	 */
	public RecordDetailer getRecord() {
		return record;
	}

	/**
	 * initializes the instance of RecordDetailer
	 */
	public StudentReader(String filename) {
		this.filename=filename;
		this.record = new RecordDetailer();
	}

	/**
	 * Method for calculating records of the file
	 * 
	 * @param i
	 */
	public void calculateRecords(IDetails i) {
		if (i == null) {
			return;
		}
		if (i.isValidRecord(record)) {
			successfulRecords++;
		} else {
			failureRecords++;
		}
	}

	/**
	 * Method for identifying the type of record
	 * 
	 * @param s
	 * @return
	 */
	public IDetails getRecordType(String s) {
		String data[] = s.split(",");
		if (data[0].equals("clg")) {
			College college = new College(data[1], data[2], data[3]);
			return record.addCollegeRecord(college);
		} else if (data[0].equals("dept")) {
			try {
				int num = Integer.parseInt(data[4]);
				Department department = new Department(data[1], data[2], data[3], num);
				return record.addDepartmentRecord(department);
			} catch (NumberFormatException e) {
				failureRecords++;
			}
		} else if (data[0].equals("stu")) {
			try {
				int num = Integer.parseInt(data[4]);
				Student student = new Student(data[1], data[2], Integer.parseInt(data[3]), num, data[5]);
				return record.addStudentRecord(student);
			} catch (NumberFormatException e) {
				failureRecords++;
			}
		}
		return null;
	}

	/**
	 * method for reading the file
	 * 
	 * @throws IOException
	 */
	public void readFile() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(filename);
		BufferedReader buffer = new BufferedReader(new InputStreamReader(in));
		String s;
		while ((s = buffer.readLine()) != null) {
			calculateRecords(getRecordType(s));
		}
		display();
	}

	/**
	 * method for displaying the total number of records and successful records and
	 * failure records
	 */
	public void display() {
		int totalRecords = successfulRecords + failureRecords;
		System.out.println("Total Records:" + totalRecords);
		System.out.println("Successful Records:" + successfulRecords);
		System.out.println("Failure Records:" + failureRecords);
	}
}
