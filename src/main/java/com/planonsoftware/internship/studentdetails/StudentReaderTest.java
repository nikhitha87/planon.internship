package com.planonsoftware.internship.studentdetails;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.planonsoftware.internship.studentdetails.recorddetails.RecordDetailer;

public class StudentReaderTest {
	private StudentReader reader;
	@Before
	public void testFile2()
	{
		 reader = new StudentReader("StudentDetails");
	}
	
	@Test
	public void testFile1() throws Exception {
		reader.readFile();
		RecordDetailer record = reader.getRecord();		
		assertEquals(2, record.getCollegeRecords().size());
		assertEquals(5,StudentReader.successfulRecords);
		assertEquals(1,StudentReader.failureRecords);
	}
}
