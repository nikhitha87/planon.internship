package com.planonsoftware.internship.studentdetails.recorddetails;

public interface IDetails {

	/**
	 * checks whether the record is valid or not
	 * 
	 * @return
	 */
	boolean isValidRecord(RecordDetailer record);
}
