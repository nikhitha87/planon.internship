package com.planonsoftware.internship.studentdetails.recorddetails;

import java.util.ArrayList;
import java.util.List;

import com.planonsoftware.internship.studentdetails.recorddetails.recordtype.College;
import com.planonsoftware.internship.studentdetails.recorddetails.recordtype.Department;
import com.planonsoftware.internship.studentdetails.recorddetails.recordtype.Student;

/**
 * @author Nikhitha
 *
 */
public class RecordDetailer {
	
	private List<College> colleges = new ArrayList<>();
	private List<Department> departments = new ArrayList<>();
	private List<Student> students = new ArrayList<>();

	/**
	 * adds the given college data into the list
	 * 
	 * @param data
	 */
	public College addCollegeRecord(College college) {

		colleges.add(college);
		return college;
	}

	/**
	 * adds the given department data into the list
	 * 
	 * @param data
	 */
	public Department addDepartmentRecord(Department department) {
		departments.add(department);
		return department;
	}

	/**
	 * adds the given student data into the list
	 * 
	 * @param data
	 */
	public Student addStudentRecord(Student student) {

		students.add(student);
		return student;
	}

	/**
	 * returns the list of colleges
	 * 
	 * @return
	 */
	public List<College> getCollegeRecords() {
		return colleges;

	}

	/**
	 * returns the list of departments
	 * 
	 * @return
	 */
	public List<Department> getDepartmentRecords() {
		return departments;

	}

	/**
	 * returns the list of students
	 * 
	 * @return
	 */
	public List<Student> getStudentRecords() {
		return students;

	}
}
