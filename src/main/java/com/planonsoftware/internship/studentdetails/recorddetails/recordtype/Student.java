package com.planonsoftware.internship.studentdetails.recorddetails.recordtype;

import com.planonsoftware.internship.studentdetails.recorddetails.IDetails;
import com.planonsoftware.internship.studentdetails.recorddetails.RecordDetailer;

public class Student implements IDetails {
	private String dCode;
	private String sName;
	private String gender;
	private int rollNo, age;

	public Student(String dCode, String sName, int rollNo, int age, String gender) {
		this.dCode = dCode;
		this.sName = sName;
		this.gender = gender;
		this.rollNo = rollNo;
		this.age = age;
	}

	/**
	 * returns the department code
	 * 
	 * @return
	 */
	public String getdCode() {
		return dCode;
	}

	public void setdCode(String dCode) {
		this.dCode = dCode;
	}

	/**
	 * returns the student name
	 * 
	 * @return
	 */
	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	/**
	 * returns the gender
	 * 
	 * @return
	 */
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * returns the rollno
	 * 
	 * @return
	 */
	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	/**
	 * returns age of student
	 * 
	 * @return
	 */
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isValidRecord(RecordDetailer record) {
		for (Department department : record.getDepartmentRecords()) {
			if ((department.getdCode()).equals(dCode)) {
				return true;
			}
		}
		return false;
	}

}
