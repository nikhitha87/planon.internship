package com.planonsoftware.internship.studentdetails.recorddetails.recordtype;

import com.planonsoftware.internship.studentdetails.recorddetails.IDetails;
import com.planonsoftware.internship.studentdetails.recorddetails.RecordDetailer;

public class Department implements IDetails {
	private String cCode;
	private String dCode;
	private String branchName;
	private int no_of_students;

	public Department(String cCode, String dCode, String branchName, int no_of_students) {
		this.cCode = cCode;
		this.dCode = dCode;
		this.branchName = branchName;
		this.no_of_students = no_of_students;
	}

	/**
	 * returns the collge code
	 * 
	 * @return
	 */
	public String getcCode() {
		return cCode;
	}

	public void setcCode(String cCode) {
		this.cCode = cCode;
	}

	/**
	 * returns the department code
	 * 
	 * @return
	 */
	public String getdCode() {
		return dCode;
	}

	public void setdCode(String dCode) {
		this.dCode = dCode;
	}

	/**
	 * returns the branch name
	 * 
	 * @return
	 */
	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * returns the no of students
	 * 
	 * @return
	 */
	public int getNo_of_students() {
		return no_of_students;
	}

	public void setNo_of_students(int no_of_students) {
		this.no_of_students = no_of_students;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isValidRecord(RecordDetailer record) {
		for (College college : record.getCollegeRecords()) {
			if ((college.getcCode()).equals(cCode)) {
				return true;
			}
		}
		return false;
	}

}
