package com.planonsoftware.internship.studentdetails.recorddetails.recordtype;

import com.planonsoftware.internship.studentdetails.recorddetails.IDetails;
import com.planonsoftware.internship.studentdetails.recorddetails.RecordDetailer;

public class College implements IDetails {
	private String cCode, cName, cAddress;

	public College(String cCode, String cName, String cAddress) {
		this.cCode = cCode;
		this.cName = cName;
		this.cAddress = cAddress;
	}

	/**
	 * returns the college code
	 * 
	 * @return
	 */
	public String getcCode() {
		return cCode;
	}

	public void setcCode(String cCode) {
		this.cCode = cCode;
	}

	/**
	 * returns the college name
	 * 
	 * @return
	 */
	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	/**
	 * returns the college address
	 * 
	 * @return
	 */
	public String getcAddress() {
		return cAddress;
	}

	public void setcAddress(String cAddress) {
		this.cAddress = cAddress;
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public boolean isValidRecord(RecordDetailer records) {
		return true;
	}

}
