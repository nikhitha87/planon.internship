package com.planonsoftware.internship.ticketingsystem;

import java.util.List;

/**
 * @author Nikhitha
 *
 */
public interface ITicket {

	/**
	 * @return returns fare
	 */
	double getTicketFare();

	/**
	 * sets the ticket details
	 */
	void setTicketDetails(List<TicketDetails> p);

	/**
	 * prints the ticket details
	 */
	void displayTicketDetails();
}
