package com.planonsoftware.internship.ticketingsystem;

import com.planonsoftware.internship.ticketingsystem.tickettype.RegularTicket;
import com.planonsoftware.internship.ticketingsystem.tickettype.SleeperTicket;
import com.planonsoftware.internship.ticketingsystem.tickettype.TicketType;

public class TicketFactory {
	/**
	 * This class validates the type of ticket to regular or sleeper and creates an
	 * object of the respective class
	 * 
	 * @param ticketType is the type of ticket
	 * @return returns the type based on the string
	 */
	public static ITicket getTicketType(String ticketType) {

		if (TicketType.REGULAR.name().equalsIgnoreCase(ticketType))  //validating enum values
			return new RegularTicket();
		else if (TicketType.SLEEPER.name().equalsIgnoreCase(ticketType))
			return new SleeperTicket();
		return null;
	}

}
