package com.planonsoftware.internship.ticketingsystem;

/**
 * @author Nikhitha
 *
 */
public class TicketDetails {
	/**
	 * members of ticket details class
	 */
	private int bookID, passengerId, pAge, no_of_tickets;
	private double fare;
	private String pName, bookingStatus;

	/**
	 * @param id
	 */
	public void setbookID(int id) {
		bookID = id;
	}

	/**
	 * @param id
	 */
	public void setpassengerId(int id) {
		passengerId = id;
	}

	/**
	 * @param id
	 */
	public void setpAge(int id) {
		pAge = id;
	}

	/**
	 * @param name
	 */
	public void setpName(String name) {
		pName = name;
	}

	/**
	 * @param id
	 */
	public void setbookingStatus(String id) {
		bookingStatus = id;
	}

	/**
	 * @param n
	 */
	public void setNo_of_Tickets(int n) {
		no_of_tickets = n;
	}

	/**
	 * @param d
	 */
	public void setFare(double d) {
		fare = d;
	}

	/**
	 * @return bookId
	 */
	public int getbookID() {
		return bookID;
	}

	/**
	 * @return getpassengerId
	 */
	public int getpassengerId() {
		return passengerId;
	}

	/**
	 * @return age
	 */
	public int getpAge() {
		return pAge;
	}

	/**
	 * @return name
	 */
	public String getpName() {
		return pName;
	}

	/**
	 * @return booking status
	 */
	public String getbookingStatus() {
		return bookingStatus;
	}

	/**
	 * @return no_of_tickets
	 */
	public int getNo_of_Tickets() {
		return no_of_tickets;
	}

	/**
	 * @return fare
	 */
	public double getFare() {
		return fare;
	}
}
