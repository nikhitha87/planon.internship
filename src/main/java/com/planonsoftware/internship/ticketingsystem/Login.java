package com.planonsoftware.internship.ticketingsystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * In this class user enters the type of class and booking is performed
 * 
 * @author Nikhitha
 *
 */
public class Login {

	public static void main(String[] args) {
		Login login = new Login();
		login.bookTicket();
	}

	/**
	 * This method takes the input and prints the output
	 */
	public void bookTicket() {
		print("********WELCOME TO TICKET BOOKING*********");
		print("Enter type of ticket(Regular/Sleeper):");
		try (Scanner scanner = new Scanner(System.in)) {
			String ticketType = scanner.next();
			// returns the object of class
			ITicket ticket = TicketFactory.getTicketType(ticketType);
			// printing result
			ticket.setTicketDetails(collectTicketDetails(ticket));
			ticket.displayTicketDetails();
		}
	}


	/**
	 * collects ticket details from users
	 * 
	 * @param ticket
	 * @return list of Ticket details
	 */
	public List<TicketDetails> collectTicketDetails(ITicket ticket) {
		try (Scanner scanner = new Scanner(System.in)) {
			List<TicketDetails> tickets = new ArrayList<TicketDetails>();
			char ch;
			do {
				TicketDetails ticketDetails = new TicketDetails();
				print("enter book id :");
				ticketDetails.setbookID(scanner.nextInt());
				print("customer id : ");
				ticketDetails.setpassengerId(scanner.nextInt());
				print("Enter Passenger Age:");
				ticketDetails.setpAge(scanner.nextInt());
				print("Enter Passenger Name:");
				ticketDetails.setpName(scanner.next());
				print("Enter Number of Tickets:");
				ticketDetails.setNo_of_Tickets(scanner.nextInt());
				print("Enter Booking Deatils(Success/Failed):");
				String status;
				while (true) {
					status = scanner.next();
					if (status.equals("success") || status.equals("failed")) {
						break;
					} else {
						System.out.println("Invalid input");
					}
				}
				ticketDetails.setbookingStatus(status);
				double totalFare = (ticket.getTicketFare() * ticketDetails.getNo_of_Tickets());
				ticketDetails.setFare(totalFare);
				tickets.add(ticketDetails);
				System.out.println("do you want to continue");
				ch = scanner.next().charAt(0);
			} while (ch == 'y' || ch == 'Y');
			return tickets;
		}
	}
	// prints the message
		public void print(String message) {
			System.out.println(message);
		}

}
