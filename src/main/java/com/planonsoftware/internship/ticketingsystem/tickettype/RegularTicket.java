package com.planonsoftware.internship.ticketingsystem.tickettype;

import java.util.List;

import com.planonsoftware.internship.ticketingsystem.ITicket;
import com.planonsoftware.internship.ticketingsystem.TicketDetails;

/**
 * @author Nikhitha
 */
public class RegularTicket implements ITicket {

	List<TicketDetails> tickets;

	/**
	 * {@inheritDoc}
	 */
	public double getTicketFare() {
		return 500.0;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setTicketDetails(List<TicketDetails> ticketDetails) {
		tickets = ticketDetails;
	}

	/**
	 * {@inheritDoc}
	 */
	public void displayTicketDetails() {
		System.out.println("Passenger details: ");
		tickets.forEach((ticketDetails) -> {
			System.out.println(ticketDetails.getbookID() + " " + ticketDetails.getpassengerId() + " "
					+ ticketDetails.getpAge() + " " + ticketDetails.getpName() + " " + ticketDetails.getbookingStatus()
					+ " " + ticketDetails.getNo_of_Tickets() + " " + ticketDetails.getFare());
		});
	}
}
