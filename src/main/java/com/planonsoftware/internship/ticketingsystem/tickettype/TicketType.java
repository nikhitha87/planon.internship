package com.planonsoftware.internship.ticketingsystem.tickettype;

/**
 * enum representing the type of tickets
 *
 */
public enum TicketType {
	REGULAR ,
    SLEEPER 
}
